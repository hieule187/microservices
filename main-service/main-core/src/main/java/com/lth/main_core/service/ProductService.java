package com.lth.main_core.service;

import com.lth.main_core.entity.Product;
import java.util.List;

public interface ProductService {

  Product create(Product product);

  List<Product> findAll();
}
