package com.lth.main_core.service.implement;

import com.lth.main_core.entity.Product;
import com.lth.main_core.repository.ProductRepository;
import com.lth.main_core.service.ProductService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceIMPL implements ProductService {

  private final ProductRepository productRepository;

  @Override
  public Product create(Product product) {
    return productRepository.save(product);
  }

  @Override
  public List<Product> findAll() {
    return productRepository.findAll();
  }
}
