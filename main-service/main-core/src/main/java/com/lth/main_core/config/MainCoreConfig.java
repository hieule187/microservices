package com.lth.main_core.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = {"com.lth.main_core.service"})
@EntityScan(basePackages = {"com.lth.main_core.entity"})
@EnableJpaRepositories(basePackages = {"com.lth.main_core.repository"})
@EnableTransactionManagement
public class MainCoreConfig {

}
