package com.lth.main_core.dto;

import lombok.Data;

@Data
public class ProductDTO {

  private String id;

  private String productName;

  private String productPrice;

  private String productQuantity;

}