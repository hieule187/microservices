package com.lth.main_core.entity;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import lombok.Data;

@Data
@Entity
public class Product {

  @Id
  private String id;

  private String productName;

  private String productPrice;

  private String productQuantity;

  public static Product of(String productName, String productPrice, String productQuantity) {
    Product product = new Product();
    product.setProductName(productName);
    product.setProductPrice(productPrice);
    product.setProductQuantity(productQuantity);

    return product;
  }

  @PrePersist
  private void ensureId() {
    if (this.getId() == null || this.getId().isEmpty()) {
      this.setId(UUID.randomUUID().toString());
    }
  }

}
