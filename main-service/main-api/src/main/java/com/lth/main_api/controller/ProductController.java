package com.lth.main_api.controller;

import com.lth.main_core.dto.ProductDTO;
import com.lth.main_core.entity.Product;
import com.lth.main_core.service.ProductService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/product")
@RequiredArgsConstructor
public class ProductController {

  private final ProductService productService;

  @PostMapping("")
  public Product create(@RequestBody @Valid ProductDTO request) {
    Product product = Product.of(
        request.getProductName(),
        request.getProductPrice(),
        request.getProductQuantity()
    );
    return productService.create(product);
  }

  @GetMapping("")
  public List<Product> findAll() {
    return productService.findAll();
  }

}
