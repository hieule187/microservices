package com.lth.main_api.config;

import com.lth.main_core.config.EnableMainCore;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableMainCore
public class MainApiConfig {

}
